<ul class="sidebar-menu">
    <li class="menu-header">Menu</li>
    <li>
        <a class="nav-link" href="#">
            <i class="fas fa-home"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Program</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ route('program.index') }}">List</a></li>
          <li><a class="nav-link" href="{{ route('program.kategori.index') }}">Kategori</a></li>
          <li><a class="nav-link" href="{{ route('program.sub_kategori.index') }}">Sub Kategori</a></li>
        </ul>
    </li>
    <li>
        <a class="nav-link" href="{{ route('kegiatan.index')}}">
            <i class="fas fa-fire"></i> <span>Kegiatan</span>
        </a>
    </li>
</ul>