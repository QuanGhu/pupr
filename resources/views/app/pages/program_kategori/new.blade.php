@extends('app.layouts.master')
@section('title','Tambah Data Program Kategori Baru')
@section('content')

<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        {!! Form::open(['id' => 'form','route' => 'program.kategori.store']) !!}
        <div class="card">
            <div class="card-header">
                Form Isian
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="">Program</label>
                    {!! Form::select('program_id', $programs, null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu']) !!}
                </div>
                <div class="form-group">
                    <label for="">Kode</label>
                    {!! Form::text('kode', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Nama</label>
                    {!! Form::text('nama', null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('program.index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $("#form").parsley()
        })
    </script>
@endpush