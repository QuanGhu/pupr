@extends('app.layouts.master')
@section('title','Tambah Data Realisasi Kegiatan Baru')
@section('content')

<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        {!! Form::open(['id' => 'form','route' => 'kegiatan.realisasi.store']) !!}
            {!! Form::hidden('kegiatan_id', $kegiatan_id, []) !!}
        <div class="card">
            <div class="card-header">
                Form Isian
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="">Nama</label>
                    {!! Form::text('name', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Isian</label>
                    {!! Form::number('value', null, ['class' => 'form-control','required','step' => '.01']) !!}
                </div>
                <div class="form-group">
                    <label for="">Tipe</label>
                    {!! Form::select('type', ['percent' => 'Persen','fixed' => 'Tetap'], null, ['class' => 'form-control','required',
                    'placeholder' => 'Pilih Salah Satu']) !!}
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('kegiatan.realisasi.index', $kegiatan_id) }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $("#form").parsley()
        })
    </script>
@endpush