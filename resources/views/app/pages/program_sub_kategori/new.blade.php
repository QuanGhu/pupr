@extends('app.layouts.master')
@section('title','Tambah Data Program Sub Kategori Baru')
@section('content')

<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        {!! Form::open(['id' => 'form','route' => 'program.sub_kategori.store']) !!}
        <div class="card">
            <div class="card-header">
                Form Isian
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="">Program</label>
                    {!! Form::select('program_id', $programs, null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu', 'id' => 'program_id']) !!}
                </div>
                <div class="form-group">
                    <label for="">Program kategori</label>
                    {!! Form::select('program_kategori_id', [], null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu','id' => 'program_kategori_id']) !!}
                </div>
                <div class="form-group">
                    <label for="">Kode</label>
                    {!! Form::text('kode', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Nama</label>
                    {!! Form::text('nama', null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('program.sub_kategori.index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $("#form").parsley()

            $("#program_id").on('change', function (e) {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    url: "{{ route('program.sub_kategori.kategori-list') }}",
                    data: {
                        program_id : $(this).val()
                    },
                    success: function(result) {
                        $("#program_kategori_id").empty();
                        $("#program_kategori_id").append(`<option value="">
                            Pilih Salah Satu
                        </option>`);

                        result.data.map(v => {
                            $("#program_kategori_id").append(`<option value="${v.id}">
                                ${v.nama}
                            </option>`);
                        })
                    },
                    error: function(err){
                        console.log(error)
                    }
                });
            })
        })
    </script>
@endpush