@extends('app.layouts.master')
@section('title','Tambah Data Kegiatan Baru')
@section('content')

<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        {!! Form::open(['id' => 'form','route' => 'kegiatan.store']) !!}
        <div class="card">
            <div class="card-header">
                Form Isian
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="">Program</label>
                    {!! Form::select('program_id', $programs, null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu', 'id' => 'program_id']) !!}
                </div>
                <div class="form-group">
                    <label for="">Program kategori</label>
                    {!! Form::select('program_kategori_id', [], null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu','id' => 'program_kategori_id']) !!}
                </div>
                <div class="form-group">
                    <label for="">Program sub kategori</label>
                    {!! Form::select('program_sub_kategori_id', [], null, ['class' => 'form-control','required',
                        'placeholder' => 'Pilih salah satu','id' => 'program_sub_kategori_id']) !!}
                </div>
                <div class="form-group">
                    <label for="">Nama</label>
                    {!! Form::text('name', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Tri Wulan</label>
                    {!! Form::select('tri_wulan', ['1' => '1', '2' => '2', '3' => '3'],null, ['class' => 'form-control','required','min' => '1']) !!}
                </div>
                <div class="form-group">
                    <label for="">Tahun</label>
                    {!! Form::number('tahun', null, ['class' => 'form-control','required','min' => '1']) !!}
                </div>
                <div class="form-group">
                    <label for="">Periode</label>
                    {!! Form::text('periode', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Tujuan</label>
                    {!! Form::textarea('tujuan', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Penjelasan</label>
                    {!! Form::textarea('penjelasan', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Pagu</label>
                    {!! Form::number('pagu', null, ['class' => 'form-control','required','min' => '1']) !!}
                </div>
                <div class="form-group">
                    <label for="">Indikator Kerja</label>
                    {!! Form::textarea('indikator_kerja', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Permasalahan</label>
                    {!! Form::textarea('permasalahan', null, ['class' => 'form-control','required']) !!}
                </div>
                <div class="form-group">
                    <label for="">Penutup</label>
                    {!! Form::textarea('penutup', null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('kegiatan.index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $("#form").parsley()

            $("#program_id").on('change', function (e) {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    url: "{{ route('kegiatan.kategori-list') }}",
                    data: {
                        program_id : $(this).val()
                    },
                    success: function(result) {
                        $("#program_kategori_id").empty();
                        $("#program_kategori_id").append(`<option value="">
                            Pilih Salah Satu
                        </option>`);

                        result.data.map(v => {
                            $("#program_kategori_id").append(`<option value="${v.id}">
                                ${v.nama}
                            </option>`);
                        })
                    },
                    error: function(err){
                        console.log(error)
                    }
                });
            })

            $("#program_kategori_id").on('change', function (e) {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    url: "{{ route('kegiatan.sub-kategori-list') }}",
                    data: {
                        program_kategori_id : $(this).val()
                    },
                    success: function(result) {
                        $("#program_sub_kategori_id").empty();
                        $("#program_sub_kategori_id").append(`<option value="">
                            Pilih Salah Satu
                        </option>`);

                        result.data.map(v => {
                            $("#program_sub_kategori_id").append(`<option value="${v.id}">
                                ${v.nama}
                            </option>`);
                        })
                    },
                    error: function(err){
                        console.log(error)
                    }
                });
            })
        })
    </script>
@endpush
