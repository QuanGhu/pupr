<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('kegiatans')) {
            Schema::create('kegiatans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('program_id');
                $table->unsignedBigInteger('program_kategori_id');
                $table->unsignedBigInteger('program_sub_kategori_id');
                $table->string('name')->nullable();
                $table->string('tri_wulan')->nullable();
                $table->string('tahun')->nullable();
                $table->string('periode')->nullable();
                $table->text('tujuan')->nullable();
                $table->text('penjelasan')->nullable();
                $table->text('pagu')->nullable();
                $table->text('indikator_kerja')->nullable();
                $table->text('permasalahan')->nullable();
                $table->text('penutup')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kegiatans');
    }
}
