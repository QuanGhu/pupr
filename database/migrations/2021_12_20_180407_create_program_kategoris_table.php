<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramKategorisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('program_kategoris')) {
            Schema::create('program_kategoris', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('kode')->nullable();
                $table->string('nama')->nullable();
                $table->unsignedBigInteger('program_id');
                $table->timestamps();

                if (Schema::hasTable('programs')) {
                    $table->foreign('program_id')->references('id')->on('programs');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_kategoris');
    }
}
