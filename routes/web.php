<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthController@index')->name('auth.index');
Route::post('/login', 'AuthController@login')->name('auth.login');
Route::get('/logout', 'AuthController@logout')->name('auth.logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    Route::group(['prefix' => 'program', 'as' => 'program.'], function () {
        Route::get('/','ProgramController@index')->name('index');
        Route::get('/new','ProgramController@new')->name('new');
        Route::get('/edit/{id}','ProgramController@edit')->name('edit');
        Route::post('/store','ProgramController@store')->name('store');
        Route::post('/update','ProgramController@update')->name('update');
        Route::delete('/delete','ProgramController@delete')->name('delete');
    });

    Route::group(['prefix' => 'program-kategori', 'as' => 'program.kategori.'], function () {
        Route::get('/','ProgramKategoriController@index')->name('index');
        Route::get('/new','ProgramKategoriController@new')->name('new');
        Route::get('/edit/{id}','ProgramKategoriController@edit')->name('edit');
        Route::post('/store','ProgramKategoriController@store')->name('store');
        Route::post('/update','ProgramKategoriController@update')->name('update');
        Route::delete('/delete','ProgramKategoriController@delete')->name('delete');
    });

    Route::group(['prefix' => 'program-sub-kategori', 'as' => 'program.sub_kategori.'], function () {
        Route::get('/','ProgramSubKategoriController@index')->name('index');
        Route::get('/new','ProgramSubKategoriController@new')->name('new');
        Route::get('/edit/{id}','ProgramSubKategoriController@edit')->name('edit');
        Route::post('/kategori-list','ProgramSubKategoriController@kategoriList')->name('kategori-list');
        Route::post('/store','ProgramSubKategoriController@store')->name('store');
        Route::post('/update','ProgramSubKategoriController@update')->name('update');
        Route::delete('/delete','ProgramSubKategoriController@delete')->name('delete');
    });

    Route::group(['prefix' => 'kegiatan', 'as' => 'kegiatan.'], function () {
        Route::get('/','KegiatanController@index')->name('index');
        Route::get('/new','KegiatanController@new')->name('new');
        Route::get('/edit/{id}','KegiatanController@edit')->name('edit');
        Route::get('/print/{id}','KegiatanController@print')->name('print');
        Route::post('/store','KegiatanController@store')->name('store');
        Route::post('/update','KegiatanController@update')->name('update');
        Route::post('/kategori-list','KegiatanController@kategoriList')->name('kategori-list');
        Route::post('/sub-kategori-list','KegiatanController@subKategoriList')->name('sub-kategori-list');
        Route::delete('/delete','KegiatanController@delete')->name('delete');

        Route::get('/{kegiatan_id}/capaian','CapaianKegiatanController@index')->name('capaian.index');
        Route::get('/{kegiatan_id}/capaian/new','CapaianKegiatanController@new')->name('capaian.new');
        Route::get('/{kegiatan_id}/capaian/edit/{id}','CapaianKegiatanController@edit')->name('capaian.edit');
        Route::post('/capaian-kegiatan/store','CapaianKegiatanController@store')->name('capaian.store');
        Route::post('/capaian-kegiatan/update','CapaianKegiatanController@update')->name('capaian.update');
        Route::delete('/capaian-kegiatan/delete','CapaianKegiatanController@delete')->name('capaian.delete');

        Route::get('/{kegiatan_id}/target','TargetKegiatanController@index')->name('target.index');
        Route::get('/{kegiatan_id}/target/new','TargetKegiatanController@new')->name('target.new');
        Route::get('/{kegiatan_id}/target/edit/{id}','TargetKegiatanController@edit')->name('target.edit');
        Route::post('/target-kegiatan/store','TargetKegiatanController@store')->name('target.store');
        Route::post('/target-kegiatan/update','TargetKegiatanController@update')->name('target.update');
        Route::delete('/target-kegiatan/delete','TargetKegiatanController@delete')->name('target.delete');

        Route::get('/{kegiatan_id}/realisasi','RealisasiKegiatanController@index')->name('realisasi.index');
        Route::get('/{kegiatan_id}/realisasi/new','RealisasiKegiatanController@new')->name('realisasi.new');
        Route::get('/{kegiatan_id}/realisasi/edit/{id}','RealisasiKegiatanController@edit')->name('realisasi.edit');
        Route::post('/realisasi-kegiatan/store','RealisasiKegiatanController@store')->name('realisasi.store');
        Route::post('/realisasi-kegiatan/update','RealisasiKegiatanController@update')->name('realisasi.update');
        Route::delete('/realisasi-kegiatan/delete','RealisasiKegiatanController@delete')->name('realisasi.delete');

        Route::get('/{kegiatan_id}/tahapan','TahapanKegiatanController@index')->name('tahapan.index');
        Route::get('/{kegiatan_id}/tahapan/new','TahapanKegiatanController@new')->name('tahapan.new');
        Route::get('/{kegiatan_id}/tahapan/edit/{id}','TahapanKegiatanController@edit')->name('tahapan.edit');
        Route::post('/tahapan-kegiatan/store','TahapanKegiatanController@store')->name('tahapan.store');
        Route::post('/tahapan-kegiatan/update','TahapanKegiatanController@update')->name('tahapan.update');
        Route::delete('/tahapan-kegiatan/delete','TahapanKegiatanController@delete')->name('tahapan.delete');

    });
});
