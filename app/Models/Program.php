<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';

    protected $fillable = [
        "kode","nama"
    ];

    public function kategories()
    {
        return $this->hasMany(ProgramKategori::class, 'program_id');
    }
}
