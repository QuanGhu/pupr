<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramKategori extends Model
{
    protected $table = 'program_kategoris';

    protected $fillable = [
        "kode","nama","program_id"
    ];

    public function program()
    {
        return $this->belongsTo(Program::class, "program_id");
    }

    public function subKategories()
    {
        return $this->hasMany(ProgramSubKategori::class, 'program_kategori_id');
    }
}
