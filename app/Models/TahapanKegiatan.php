<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TahapanKegiatan extends Model
{
    protected $table = 'tahapan_kegiatans';

    protected $fillable = [
        'kegiatan_id','deskripsi'
    ];

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'kegiatan_id');
    }
}
