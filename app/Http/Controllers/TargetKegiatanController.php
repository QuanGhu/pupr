<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TargetKegiatan;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;

class TargetKegiatanController extends Controller
{
    public function index(Builder $builder, $kegiatan_id)
    {
        if (request()->ajax()) {
			return $this->datatables($kegiatan_id);
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.target_kegiatan.index')->with([
            'html' => $html,
            'kegiatan_id' => $kegiatan_id
        ]);
    }

    public function datatables($kegiatan_id)
	{
        $table = new TargetKegiatan;
        return datatables($table->where('kegiatan_id', $kegiatan_id)->get())
        ->addIndexColumn()
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='"
                    .route('kegiatan.target.edit', ['kegiatan_id' => $model->kegiatan_id,'id' => $model->id])."'><i class='fas fa-edit'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'name', 'title' => 'Nama'
            ],
            [
                'data' => 'value', 'title' => 'Value'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new($kegiatan_id)
    {
        return view('app.pages.target_kegiatan.new')
            ->with([
                "kegiatan_id" => $kegiatan_id
            ]);
    }

    public function edit($kegiatan_id, $id)
    {
        $data = TargetKegiatan::findOrFail($id);

        return view('app.pages.target_kegiatan.edit')
            ->with([
                "data" => $data,
                "kegiatan_id" => $kegiatan_id
            ]);
    }

    public function store(Request $request)
    {
        $store = TargetKegiatan::create($request->all());

        return $store ? redirect()->route('kegiatan.target.index', $request->kegiatan_id)->with('success', 'Data berhasil disimpan')
            : redirect()->route('kegiatan.target.index',$request->kegiatan_id)->with('danger', 'Data gagal tersimpan');
    }

    public function update(Request $request)
    {
        $data = TargetKegiatan::findOrFail($request->id);
        $data->kegiatan_id = $request->kegiatan_id;
        $data->name = $request->name;
        $data->value = $request->value;
        $data->type = $request->type;
        $store = $data->save();

        return $store ? redirect()->route('kegiatan.target.index',$request->kegiatan_id)->with('success','Data berhasil disimpan')
            : redirect()->route('kegiatan.target.index',$request->kegiatan_id)->with('danger','Data gagal disimpan');
    }

    public function delete(Request $request)
    {
        $data = TargetKegiatan::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }
}
