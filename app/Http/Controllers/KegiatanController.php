<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;
use App\Models\ProgramKategori;
use App\Models\ProgramSubKategori;
use App\Models\Kegiatan;
use App\Models\CapaianKegiatan;
use App\Models\RealisasiKegiatan;
use App\Models\TahapanKegiatan;
use App\Models\TargetKegiatan;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;
use PDF;
use NumberToWords\NumberToWords;

class KegiatanController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
			return $this->datatables();
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.kegiatan.index')->with([
            'html' => $html
        ]);
    }

    public function datatables()
	{
        $table = new Kegiatan;
        return datatables($table->query())
        ->addIndexColumn()
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='".route('kegiatan.edit', $model->id)."'><i class='fas fa-edit'></i></a>
                    <a class='btn btn-sm btn-warning' href='".route('kegiatan.print', $model->id)."' target='__blank'><i class='fas fa-print'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                    <div class='dropdown'>
                        <button class='btn btn-info dropdown-toggle' type='button' 
                            id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <i class='fas fa-cog'></i>
                        </button>
                        <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                            <a class='dropdown-item' href='".route('kegiatan.capaian.index', $model->id)."'>Atur Capaian</a>
                            <a class='dropdown-item' href='".route('kegiatan.realisasi.index', $model->id)."'>Atur Realisasi</a>
                            <a class='dropdown-item' href='".route('kegiatan.tahapan.index', $model->id)."'>Atur Tahapan</a>
                            <a class='dropdown-item' href='".route('kegiatan.target.index', $model->id)."'>Atur Target</a>
                        </div>
                    </div>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'name', 'title' => 'Nama'
            ],
            [
                'data' => 'tujuan', 'title' => 'Tujuan'
            ],
            [
                'data' => 'tri_wulan', 'title' => 'Tri Wulan'
            ],
            [
                'data' => 'tahun', 'title' => 'Tahun'
            ],
            [
                'data' => 'periode', 'title' => 'Periode'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new()
    {
        $programs = Program::pluck('nama','id');
        return view('app.pages.kegiatan.new')
            ->with([
                "programs" => $programs
            ]);
    }

    public function edit($id)
    {
        $programs = Program::pluck('nama','id');
        $data = Kegiatan::findOrFail($id);

        return view('app.pages.kegiatan.edit')
            ->with([
                "programs" => $programs,
                "data" => $data
            ]);
    }

    public function store(Request $request)
    {
        $store = Kegiatan::create($request->all());

        return $store ? redirect()->route('kegiatan.index')->with('success', 'Data berhasil disimpan')
            : redirect()->route('kegiatan.index')->with('danger', 'Data gagal tersimpan');
    }

    public function update(Request $request)
    {
        $data = Kegiatan::findOrFail($request->id);
        $data->program_id = $request->program_id;
        $data->program_kategori_id = $request->program_kategori_id;
        $data->program_sub_kategori_id = $request->program_sub_kategori_id;
        $data->name = $request->name;
        $data->tri_wulan = $request->tri_wulan;
        $data->tahun = $request->tahun;
        $data->periode = $request->periode;
        $data->tujuan = $request->tujuan;
        $data->penjelasan = $request->penjelasan;
        $data->pagu = $request->pagu;
        $data->indikator_kerja = $request->indikator_kerja;
        $data->permasalahan = $request->permasalahan;
        $data->penutup = $request->penutup;
        $store = $data->save();

        return $store ? redirect()->route('kegiatan.index')->with('success','Data berhasil disimpan')
            : redirect()->route('kegiatan.index')->with('danger','Data gagal disimpan');
    }

    public function kategoriList(Request $request)
    {
        $datas = ProgramKategori::where('program_id', $request->program_id)->get();

        return response()->json([
            "status" => true,
            "data" => $datas
        ]);
    }

    public function subKategoriList(Request $request)
    {
        $datas = ProgramSubKategori::where('program_kategori_id', $request->program_kategori_id)->get();

        return response()->json([
            "status" => true,
            "data" => $datas
        ]);
    }

    public function delete(Request $request)
    {
        $data = Kegiatan::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }

    public function print($id)
    {
        $data = Kegiatan::findOrFail($id);
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('id');

        $pdf = PDF::loadView('app.pdf.kegiatan', ['data' => $data, 'numberTransformer' => $numberTransformer])->setPaper('F4', 'portrait');
        return $pdf->stream();
    }
}
