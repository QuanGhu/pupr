<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProgramKategori;
use App\Models\ProgramSubKategori;
use App\Models\Program;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;

class ProgramSubKategoriController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
			return $this->datatables();
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.program_sub_kategori.index')->with([
            'html' => $html
        ]);
    }

    public function datatables()
	{
        $table = new ProgramSubKategori;
        return datatables($table->query())
        ->addIndexColumn()
        ->addColumn('kategori', function($model) {
            return $model->kategori->nama;
        })
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='".route('program.sub_kategori.edit', $model->id)."'><i class='fas fa-edit'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'kode', 'title' => 'Kode'
            ],
            [
                'data' => 'nama', 'title' => 'Nama'
            ],
            [
                'data' => 'kategori', 'title' => 'Kategori'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new()
    {
        $programs = Program::pluck('nama','id');
        return view('app.pages.program_sub_kategori.new')
            ->with([
                "programs" => $programs
            ]);
    }

    public function edit($id)
    {
        $data = ProgramSubKategori::findOrFail($id);
        $programs = Program::pluck('nama','id');
        return view('app.pages.program_sub_kategori.edit')
            ->with([
                "data" => $data,
                "programs" => $programs
            ]);
    }

    public function store(Request $request)
    {
        $store = ProgramSubKategori::create([
            "kode" => $request->kode,
            "nama" => $request->nama,
            "program_kategori_id" => $request->program_kategori_id,
        ]);

        return $store ? redirect()->route('program.sub_kategori.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.kategori.index')->with('danger','Data gagal disimpan');
    }

    public function update(Request $request)
    {
        $data = ProgramSubKategori::findOrFail($request->id);
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->program_kategori_id = $request->program_kategori_id;
        $store = $data->save();

        return $store ? redirect()->route('program.sub_kategori.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.sub_kategori.index')->with('danger','Data gagal disimpan');
    }

    public function delete(Request $request)
    {
        $data = ProgramSubKategori::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }

    public function kategoriList(Request $request)
    {
        $datas = ProgramKategori::where('program_id', $request->program_id)->get();

        return response()->json([
            "status" => true,
            "data" => $datas
        ]);
    }
}
