<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProgramKategori;
use App\Models\Program;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Validator;

class ProgramKategoriController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
			return $this->datatables();
        }

        $html = $this->builder($builder)
			->minifiedAjax()
            ->responsive()
            ->autoWidth(false);

        return view('app.pages.program_kategori.index')->with([
            'html' => $html
        ]);
    }

    public function datatables()
	{
        $table = new ProgramKategori;
        return datatables($table->query())
        ->addIndexColumn()
        ->addColumn('program', function($model) {
            return $model->program->nama;
        })
        ->addColumn('action', function($model) {
            return "
                <div class='btn-group'>
                    <a class='btn btn-sm btn-primary' href='".route('program.kategori.edit', $model->id)."'><i class='fas fa-edit'></i></a>
                    <button class='btn btn-sm btn-danger btn-delete'>
                        <i class='fas fa-trash'></i>
                    </button>
                </div>
            ";
            return "";
        })
        ->toJson();
    }

    public function builder(Builder $builder)
	{
		return $builder->columns([
            [
                'data' => 'DT_RowIndex','title' => '#',
                'orderable' => false,'searchable' => false,
                'width' => '24px'
            ],
            [
                'data' => 'kode', 'title' => 'Kode'
            ],
            [
                'data' => 'nama', 'title' => 'Nama'
            ],
            [
                'data' => 'program', 'title' => 'Program'
            ],
            [
                'data' => 'action','title' => 'Action',
                'width' => '120px','class' => 'text-center',
                'orderable' => false,
                'searchable' => false
            ]
		]);
    }

    public function new()
    {
        $programs = Program::pluck('nama','id');
        return view('app.pages.program_kategori.new')
            ->with([
                "programs" => $programs
            ]);
    }

    public function edit($id)
    {
        $data = ProgramKategori::findOrFail($id);
        $programs = Program::pluck('nama','id');
        return view('app.pages.program_kategori.edit')
            ->with([
                "data" => $data,
                "programs" => $programs
            ]);
    }

    public function store(Request $request)
    {
        $store = ProgramKategori::create([
            "kode" => $request->kode,
            "nama" => $request->nama,
            "program_id" => $request->program_id,
        ]);

        return $store ? redirect()->route('program.kategori.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.kategori.index')->with('danger','Data gagal disimpan');
    }

    public function update(Request $request)
    {
        $data = ProgramKategori::findOrFail($request->id);
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->program_id = $request->program_id;
        $store = $data->save();

        return $store ? redirect()->route('program.kategori.index')->with('success','Data berhasil disimpan')
            : redirect()->route('program.kategori.index')->with('danger','Data gagal disimpan');
    }

    public function delete(Request $request)
    {
        $data = ProgramKategori::where('id', $request->id)->firstOrFail();
        $delete = $data->delete();
        
        return $delete ? response()->json(['success' => true,'message'=>'Data berhasil dihapus'])
        : response()->json(['success' => false,'message'=>'Data gagal dihapus']);
    }
}
